# IMPORTANT

<span style="color: red">**Please do not leave this application running after testing. There is NO SECURITY.**</span>

## Introduction

This is a test app to show the issue reported under [2309220030000250](https://portal.azure.com/#view/Microsoft_Azure_Support/SupportRequestDetails.ReactView/id/%2Fsubscriptions%2F79bc921d-f511-4d4e-a594-2d93196d2768%2Fproviders%2FMicrosoft.Support%2FsupportTickets%2F2e9d497d-06cebca6-8631fec8-ca53-4b52-9a41-73d486ced07c) - Participants can't see screen share when using azsdk-js-communication-calling/1.17.1-beta.5

## Usage

### Installation

After cloning the repository, you need to add a `.env` to the root directory and fill in the `CLIENT_ID`, `CLIENT_SECRET` and `COMMUNICATION_SERVICES_CONNECTION_STRING`:

```
REDIRECT_URI="http://localhost:3000/api/auth/redirect"
CLIENT_ID="«client id»"
CLIENT_SECRET="«client secret»"
AUTHORITY="https://login.microsoftonline.com/common"
COMMUNICATION_SERVICES_CONNECTION_STRING="«communication services connection string»"
VITE_COOKIE_NAME="ms-teams-screen-share-issue"
```

Install the required node modules and start:

```
npm i
npm run dev
```

### Running The App

Open the app at [http://localhost:3000/](http://localhost:3000/)

- Create a MS Teams meeting and get the URL for the meeting
- Open the web page, enter your email address and click `update`.
- Enter the meeting URL.
- Choose the type of user:
	- Click **authenticate** to authenticate as an MS Teams user
	- Click **guest token** to join the meeting as a guest
- Click **join** to join the call
