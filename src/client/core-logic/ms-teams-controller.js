// Azure
import { CallClient } from '@azure/communication-calling';
import { AzureCommunicationTokenCredential } from '@azure/communication-common';
import { AzureLogger, setLogLevel } from '@azure/logger';
import { ref, toValue } from 'vue';

class MSTeamsController {
  constructor(logLevel) {
    console.debug(`MSTeamsController.constructor() logLevel-[${logLevel}]`);

    this._communicationToken = null;
    this.isGuest = false;

    this._meeting = null;

    this.teamsCallAgent = null;

    this.displayName = null;

    this.setLogLevel(logLevel);
    AzureLogger.log = (...args) => {
      // Extract the level string from the logs
      const matches = (args[0].match(/azure:ACS-calling:(\S+)/) || []);
      const [ , level ] = matches;
    
      const log = {
        error: (...args) => console.error(...args),
        info: (...args) => console.log(...args),
        verbose: (...args) => console.debug(...args),
        warning: (...args) => console.warn(...args)
      };
      
      log?.[level](...args);
    };
  }

  setLogLevel(logLevel) {
    console.debug(`setLogLevel() logLevel-[${logLevel}]`);
    if (!['error', 'info', 'verbose', 'warning'].includes(logLevel)) return;
    setLogLevel(logLevel);
  }

  async init(communicationToken, guest = false, username, displayName) {
    console.debug(`MSTeamsController.init() communicationToken-[${communicationToken}] guest-[${guest}] username-[${username}] displayName-[${displayName}]`);
    this._communicationToken = communicationToken;
    this.isGuest = guest;
    this.username = username;
    this.displayName = displayName;
    
    await this.initializeCallClient();
  }

  /** @return {string} The users teams id */
  getClientId() {
    return this.teamsCallAgent?.getClientId();
  }

  /**
   * @type {import('@azure/communication-calling').Call}
   * @type {import('@azure/communication-calling').JoinCallOptions}
   *
   * @param {string} meetingLink The meeting URL
   * @param {JoinCallOptions} [config={}] Configuration options...
   * @returns {Call}
   */
  joinMeeting(meetingLink, config = {}) {
    console.debug(`MSTeamsController.joinMeeting() meetingLink-[${meetingLink}] - config`, config);
    this._meeting = this.teamsCallAgent.join({ meetingLink }, config);
    return this._meeting;
  }

  /** @returns {Promise} Resolved when the call has hung up */
  hup() {
    console.debug('MSTeamsController.hup()');
    if (!this._meeting) return;
    return this._meeting.hangUp();
  }

  async initializeCallClient() {
    console.debug(`MSTeamsController._initializeCallClient() this._communicationToken-[${this._communicationToken}]`);

    try {
      // Pass this to the CallClient() if diagnostics is required
      // {
      //   diagnostics: {
      //     appName: 'Betr',
      //     appVersion: '1.0',
      //     tags: [ 'diagnostics' ]
      //   }
      // }

      // this._callClient = new CallClient({});
      this._callClient = new CallClient({
        diagnostics: {
          appName: 'Betr',
          appVersion: '1.0',
          tags: [ 'diagnostics' ]
        }
      });
      console.debug(`MSTeamsController._initializeCallClient() this.isGuest-[${this.isGuest}] sdkDiagnosticInformation-${this._callClient?.sdkDiagnosticInformation}`);

      const tokenCredential = new AzureCommunicationTokenCredential(this._communicationToken.trim());

      if (this.isGuest) {
        this.teamsCallAgent = await this._callClient.createCallAgent(tokenCredential, { displayName: this.displayName });
      } else {
        // The displayName can't be set for a Teams user
        this.teamsCallAgent = await this._callClient.createTeamsCallAgent(tokenCredential);
      }
    } catch (error) {
      console.debug(`MSTeamsController._initializeCallClient() error.message-[${error.message}]`, error);
    }
  }
}

export { MSTeamsController };
