import { RemoteTrack, TRACK_TYPE } from './remote-track';

class RemoteParticipant {
	constructor(participant, updateCb) {
    console.debug('RemoteParticipant.constructor() participant', participant);
		this._raw = participant;

		this.id = RemoteParticipant.ParticipantId(participant);
		this.displayName = participant.displayName;
    this.state = participant?.state;

		this._tracks = [];

		this.videoStream = null;
		this.videoAvailable = false;
		this.screenShareStream = null;
		this.screenShareAvailable = false;

    this._addVideoStreams(participant.videoStreams);

		this._eventListeners = [];
		this._addEventListeners();

		this.ROOM_updateCb = updateCb;
	}

  get identifier() {
    return this._raw.identifier;
  }

	// ---------- PUBLIC ----------

	getVideoMediaStream() {
		return this.videoStream.mediaStream;
	}

	getScreenShareMediaStream() {
		return this.screenShareStream.mediaStream;
	}

	// ---------- TRACK - PRIVATE ----------

  /** @param {Array<RemoteVideoStream>} videoStreams Add the passed video streams */
  _addVideoStreams(videoStreams){
    console.debug('RemoteParticipant._addVideoStreams() videoStreams', videoStreams);

    videoStreams.forEach(videoStream => {
      console.debug(`RemoteParticipant._addVideoStreams() videoStream.id-[${videoStream.id}] videoStream`, videoStream);
			const track = new RemoteTrack(videoStream, this._updatedCb.bind(this));

      console.debug(`RemoteParticipant._addVideoStreams() track`, track);
      this._tracks.push(track);

			if (track.type === TRACK_TYPE.VIDEO) this.videoStream = track;
			if (track.type === TRACK_TYPE.SCREEN) this.screenShareStream = track;
    });
  }

  /** @param {Array<RemoteVideoStream>} videoStreams Remove the passed video streams */
  _removeVideoStreams(videoStreams) {
    console.debug('RemoteParticipant._removeVideoStreams() videoStreams', videoStreams);

    videoStreams.forEach(videoStream => {
      console.debug(`RemoteParticipant._removeVideoStreams() videoStream.id-[${videoStream.id}] videoStream`, videoStream);
			const idx = this._tracks.findIndex(track => track.id === videoStream.id);
			const track = this._tracks[idx];
			if (idx === -1) return;
			this._tracks.splice(idx, 1);

			if (track.type === TRACK_TYPE.VIDEO) this.videoStream = null;
			if (track.type === TRACK_TYPE.SCREEN) this.screenShareStream = null;
    });
  }

	_updatedCb(track) {
		console.debug(`RemoteParticipant._updateCb() track`, track);

		const updateFns = Object.freeze({
			[TRACK_TYPE.VIDEO]: track => this.videoAvailable = track.isAvailable,
			[TRACK_TYPE.SCREEN]: track => this.screenShareAvailable = track.isAvailable
		});

		updateFns?.[track.type]?.(track);

		this.ROOM_updateCb(this);
	}

	// ---------- EVENTS ----------

  /** Add required listeners for RemoteParticipant events */
  _addEventListeners() {
    this._addEventListener('videoStreamsUpdated', this._onVideoStreamsUpdated.bind(this));
    this._addEventListener('isMutedChanged', this._onIsMutedChanged.bind(this));
    this._addEventListener('stateChanged', this._onStateChanged.bind(this, 'stateChanged'));
  }

  /**
   * Register the listener for the event and store the listener so it can be removed at a later date.
   * @param {string} eventName The event to register the listener for
   * @param {function} listener The callback function
   */
  _addEventListener(eventName, listener) {
    this._raw?.on(eventName, listener);
    this._eventListeners.push({ eventName, listener });
  }

  /** Removes all event listeners */
  _removeEventListeners() {
    this._eventListeners.forEach(({ eventName, listener }) => this._raw?.off(eventName, listener));
    this._eventListeners = [];
  }

  _onVideoStreamsUpdated({ added = [], removed = [] }) {
    console.debug('MSTeamsParticipantRemote._onVideoStreamsUpdated() added/removed', { added, removed });

    this._addVideoStreams(added);
    this._removeVideoStreams(removed);

    this._updatedCb(this);
  }

  /** Handle the isMuteChanged event - call the callback */
  _onIsMutedChanged() {
    this._updatedCb(this);
  }

  /** Handle the stateChanged event */
  _onStateChanged() {
    this.state = this._raw?.state ?? 'UNKNOWN';
    console.debug(`MSTeamsParticipantRemote._onStateChanged() state-[${this.state}] this.id-[${this.id}]`);
    this._updatedCb(this);
  }

	// ---------- STATIC ----------

  /**
   * @throws {Error} If the user type is unknown
   *
   * @param {RemoteParticipant} participant The participant from MS Teams SDK
   * @returns {string|null} The participants id used in the conference or null if not found
   */
  static ParticipantId(participant) {
    const identifier = participant?.identifier;

    // MicrosoftTeamsUserKind
    if (identifier?.kind === 'microsoftTeamsUser') {
      return identifier.rawId;
    }

    // CommunicationUserKind
    else if (identifier?.kind === 'communicationUser') {
      return identifier.communicationUserId;
    }

    return null;
  }
}

export { RemoteParticipant };
