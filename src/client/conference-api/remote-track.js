const TRACK_TYPE = Object.freeze({
	VIDEO: 'VIDEO',
	SCREEN: 'SCREEN'
});

class RemoteTrack {
  /**
   * @param {RemoteVideoStream} videoStream The MS Teams SDK remote video stream
   * @param {UpdatedCb} updatedCb A callback to call when this participant is updated
   */
  constructor(videoStream, updateCb) {
    console.debug('RemoteTrack.constructor(): videoStream', videoStream);

    this._eventListeners = [];
    this.mediaStream = null;
    this._raw = videoStream;
    this._updateCb = updateCb;

    this.id = String(this._raw?.id);

    this.type = videoStream.mediaStreamType === 'Video'
      ? TRACK_TYPE.VIDEO
      : videoStream.mediaStreamType === 'ScreenSharing'
        ? TRACK_TYPE.SCREEN
        : TRACK_TYPE.UNKNOWN;

    this._addEventListeners();

		this.isAvailable = this._raw?.isAvailable;
		this.isReceiving = this._raw?.isReceiving;

		this.configureMediaStream()
			.then(() => {
				if (this._updateCb) this._updateCb(this);
			});
  }

  /** Remove any event listeners and clean up any VideoStreamRenderer/VideoStreamRendererView objects */
  cleanUp() {
    console.debug(`RemoteTrack.cleanUp: id=[${this.id}] type=[${this._type}]`);
    this._removeEventListeners();
    this._removeVideoRenderer();
  }

  // ---------- PUBLIC ----------

  async asyncMediaStream() {
    console.debug(`RemoteTrack.asyncMediaStream() sid=[${this.sid}] type=[${this.type}]`);
    return this.mediaStream;
  }

  // ---------- PRIVATE ----------

  /** Add event listeners for isAvailableChanged and isReceivingChanged */
  _addEventListeners() {
    console.debug('RemoteTrack._addEventListeners()');
    this._addEventListener('isAvailableChanged', this._onIsAvailableChanged.bind(this));
    this._addEventListener('isReceivingChanged', this._onIsReceivingChanged.bind(this));
  }

  /**
   * Register the listener for the event and store the listener so it can be removed at a later date.
   * @param {string} eventName The event to register the listener for
   * @param {function} listener The callback function
   */
  _addEventListener(eventName, listener) {
    this._raw?.on(eventName, listener);
    this._eventListeners.push({ eventName, listener });
  }

  /** Removes all event listeners */
  _removeEventListeners() {
    this._eventListeners.forEach(({ eventName, listener }) => this._raw?.off(eventName, listener));
    this._eventListeners = [];
  }

  // ---------- PRIVATE EVENT LISTENERS ----------

  /** Event handler for isAvailableChanged, simply calls the update callback */
  _onIsAvailableChanged() {
    console.debug(`RemoteTrack._onIsAvailableChanged: isAvailable-[${this._raw?.isAvailable}]`);

    // Handle creation/removal of the MediaStream, then fire the update callback
    this.configureMediaStream()
      .then(() => {
				this.isAvailable = this._raw?.isAvailable;
				if (this._updateCb) this._updateCb(this);
			});
  }

  /** Event handler for isReceivingChanged, simply calls the update callback */
  _onIsReceivingChanged() {
    console.debug(`RemoteTrack._onIsReceivingChanged: isReceiving-[${this._raw?.isReceiving}]`);

		this.isReceiving = this._raw?.isReceiving;
		if (this._updateCb) this._updateCb(this);
  }

  // ---------- PRIVATE - MEDIA STREAMS ----------

  async configureMediaStream() {
    console.debug(`RemoteTrack.configureMediaStream()`);

    if (this.mediaStream) {
      this.mediaStream = null;
    }

		if (!this._raw?.isAvailable) return;

		try {
			this.mediaStream = await this._raw.getMediaStream();
		} catch (error) {
			console.error(`RemoteTrack.configureMediaStream() ERROR: error.message-[${error.message}]`, error);
		}
	}
}

export { RemoteTrack, TRACK_TYPE };
