import { Features } from '@azure/communication-calling';

const commands = Object.freeze({
	START_SCREEN_SHARE: 'START_SCREEN_SHARE',
	START_SCREEN_SHARE: 'START_SCREEN_SHARE_WITH_VIDEO'
});

class DataChannelManager {
	constructor(meeting, startScreenShareCb) {
		console.log('DataChannelManager.constructor()');
		this._meeting = meeting;

		this._startScreenShareCb = startScreenShareCb;

		this._dataChannel = this._meeting.feature(Features.DataChannel);
		this._messageSender = this._dataChannel.createDataChannelSender({ channelId: 1000 });
		this._messageSender.setParticipants([]);

		this._dataChannel.on('dataChannelReceiverCreated', this._onDataChannelReceiverCreated.bind(this));
	}

	_onDataChannelReceiverCreated(receiver) {
		console.log('DataChannelManager._onDataChannelReceiverCreated() - receiver', receiver);
    if (receiver.channelId === 1000) {
			const textDecoder = new TextDecoder();
			receiver.on('close', () => {
					console.log(`data channel id = ${receiver.channelId} from ${JSON.stringify(receiver.senderParticipantIdentifier)} is closed`);
			});
			receiver.on('messageReady', () => {
					const { data } = receiver.readMessage();
					
					try {
						const message = JSON.parse(textDecoder.decode(data));
						console.debug(`DataChannelManager onMessageReady - message`, message);

						const fns = Object.freeze({
							[commands.START_SCREEN_SHARE]: () => this._startScreenShareCb(),
							[commands.START_SCREEN_SHARE_WITH_VIDEO]: () => this._startScreenShareCb(true)
						});
						if (Object.values(commands).includes(message?.command)) fns[message?.command]();
					} catch (error) {
						console.error(`ERROR - error.message-[${error.message}]`, error);
					}
			});
		}
	}

	_sendMessage(msg) {
		console.debug(`DataChannelManager._sendMessage() - msg`, msg);

		const data = new TextEncoder().encode(JSON.stringify(msg));
		this._messageSender.sendMessage(data);
	}

	// ---------- PUBLIC ----------

	sendStartScreenShare(withVideo = false) {
		const command = withVideo ? commands.START_SCREEN_SHARE_WITH_VIDEO : commands.START_SCREEN_SHARE;
		this._sendMessage({ command });
	}
}

export { DataChannelManager }