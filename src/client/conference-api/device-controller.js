const defaultVideoConstraints = Object.freeze({
	height: { min: 480, ideal: 720, max: 720 },
	width: { min: 640, ideal: 1280, max: 1280 }
});

class DeviceController {
	constructor() {}

  async getMediaStream(audio, video, audioDeviceId = null, videoDeviceId = null, videoConstraints = defaultVideoConstraints) {
    console.debug(`DeviceController.getMediaStream audio-[${audio}]-[${audioDeviceId}] video-[${video}]-[${videoDeviceId}]`);

    // Get audio and video constraints as required
    const constraints = { audio, video };
    if (audioDeviceId) constraints.audio = { deviceId: audioDeviceId };
    if (video) {
      if (videoConstraints) constraints.video = videoConstraints;
      if (videoDeviceId) constraints.video.deviceId = videoDeviceId;
    }
    console.debug('DeviceController.getMediaStream()', constraints);

		return navigator.mediaDevices.getUserMedia(constraints);
  }

	/**
   * Returns a MediaStream containing a screen share (used by browsers)
   * @return {Promise<MediaStream>} A promise to a MediaStream containing the screen share
   */
	getScreenShare() {
		console.debug('DeviceController.getScreenShare()');

		return navigator.mediaDevices.getDisplayMedia();
	}
	
  async requestDevicePermissions(audio, video) {
    console.debug(`DeviceController.requestDevicePermissions audio-[${audio}] video-[${video}]`);

    try {
      // Request only REQUIRED audio and video permissions
      const stream = await this.getMediaStream(audio, video);
      (stream?.getTracks() ?? []).forEach(track => track.stop());
    } catch (error) {
      console.debug(`DeviceController.requestDevicePermissions error.message-[${error.message}]`, error);
    }
  }

  async getAudioInputDevices() {
    const devices = await navigator.mediaDevices.enumerateDevices();

    return devices.filter(({ kind }) => kind === 'audioinput');
  }
}

export { DeviceController }
