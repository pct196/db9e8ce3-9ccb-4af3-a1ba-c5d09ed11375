import { LocalAudioStream, LocalVideoStream } from '@azure/communication-calling';
import { markRaw, reactive } from 'vue';
import { RemoteParticipant } from './remote-participant';
import { DataChannelManager } from './data-channel-manager';

class MSTeamsRoom {
  constructor() {
    console.debug('MSTeamsRoom.constructor()');

    this.deviceController = null;
    this.msTeamsController = null;

    this.audioMediaStream = null;
    this.localAudioStream = null;

    this.videoMediaStream = null;
    this.localVideoStream = null;

    this.screenShareMediaStream = null;
    this.localScreenShareStream = null;

    this.meeting = null;

    this._eventHandlers = [];

    this.data = reactive({
      state: '',
      participants: [],
      audioInputDevices: [],
      selectedAudioInputDeviceId: null
    });

    this._dataChannelManager = null;

    this.remoteParticipants = new Map();
  }

  init(configuration = {}) {
    this.deviceController = configuration.deviceController;
    this.msTeamsController = configuration.msTeamsController;

    this.getAudioInputDevices();
  }

  async admit(id) {
    const participant = this.remoteParticipants.get(id);
    console.debug('MSTeamsRoom.admit() - participant', participant, 'participant.identifier', participant?.identifier);
    
    if (!participant) return;
    await this.meeting?.lobby?.admit(participant.identifier);
  }

  async setSelectedAudioInputDeviceIdFromLocalAudioStream() {
    const stream = await this.localAudioStream?.getMediaStream();
    console.debug('setSelectedAudioInputDeviceIdFromLocalAudioStream() - stream', stream);
    if (!stream) return;

    // Update the selected audio deviceId from the LocalAudioStream
    this.data.selectedAudioInputDeviceId = stream?.getAudioTracks()?.at(0)?.getSettings()?.deviceId;
    console.debug(`setSelectedAudioInputDeviceIdFromLocalAudioStream() - stream?.getAudioTracks()?.at(0)?.getSettings()?.deviceId-[${stream?.getAudioTracks()?.at(0)?.getSettings()?.deviceId}]`);
  }

  async setAudioInputDevice(device) {
    console.debug(`MSTeamsRoom.audioInputDeviceClicked() device.label-[${device.label}] device.deviceId-[${device.deviceId}]`);

    const oldMediaStream = this.audioMediaStream;
    try {
      this.audioMediaStream = await this.deviceController.getMediaStream(true, false, device.deviceId);

      const deviceId = ((await this.audioMediaStream.getAudioTracks()) ?? []).at(0)?.getSettings()?.deviceId;
      console.debug(`setAudioInputDevice() created MediaStream using deviceId-[${deviceId}]`);
      
      await this.localAudioStream.setMediaStream(this.audioMediaStream);
    } catch (error) {
      console.error(`ERROR - error.message-[${error.message}] - error`, error);
    } finally {
      (oldMediaStream?.getAudioTracks() ?? []).forEach(track => track.stop());
      await this.setSelectedAudioInputDeviceIdFromLocalAudioStream();
    }
  }

  async join(meetingId, startAudioMuted = true, startAudio = true, startVideo = false) {
    console.debug(`MSTeamsRoom.join() startAudio-[${startAudio}] startVideo-[${startVideo}] startAudioMuted-[${startAudioMuted}]`);

    // Configure audio if required
    let audioOptions = null;
    if (startAudio) {
      this.audioMediaStream = await this.deviceController.getMediaStream(true, false);
      this.localAudioStream = new LocalAudioStream(this.audioMediaStream);
      audioOptions = {
        localAudioStreams: [ this.localAudioStream ],
        muted: startAudioMuted
      };
    }

    await this.setSelectedAudioInputDeviceIdFromLocalAudioStream();

    // Configure video if required
    let videoOptions = null;
    if (startVideo) {
      this.videoMediaStream = await this.deviceController.getMediaStream(false, true);
      this.localVideoStream = new LocalVideoStream(this.videoMediaStream);
      videoOptions = {
        localVideoStreams: [ this.localVideoStream ]
      };
    }

    const options = {
      ...(startAudio && { audioOptions }),
      ...(startVideo && { videoOptions })
    }

    console.debug(`MSTeamsRoom.join() - join meeting meetingId-[${meetingId}] - options`, options);

    this.meeting = this.msTeamsController.joinMeeting(meetingId, options);

    this._registerEventHandlers();

    this._createDataChannel();
  }

  async leave() {
    const idx = this.data.participants.findIndex(participant => participant.localParticipant ?? false);
    if (idx !== -1) this.data.participants.splice(idx, 1);

    this.msTeamsController.hup();

    this.localAudioStream = null;
    (this.audioMediaStream?.getAudioTracks() ?? []).forEach(track => track.stop());
    this.audioMediaStream = null;

    this.localVideoStream = null;
    (this.videoMediaStream?.getVideoTracks() ?? []).forEach(track => track.stop());
    this.videoMediaStream = null;

    this.localScreenShareStream = null;
    (this.screenShareMediaStream?.getVideoTracks() ?? []).forEach(track => track.stop());
    this.screenShareMediaStream = null;
  }

  getVideoMediaStream() {
    return this.videoMediaStream;
  }

  getScreenShareMediaStream() {
    return this.screenShareMediaStream;
  }

  async startScreenShare(withVideo = false) {
    const isScreenSharingOn = this.meeting.isScreenSharingOn;
    console.log(`startScreenShare() - withVideo-[${withVideo}] isScreenSharingOn-[${isScreenSharingOn}]`);

    if (isScreenSharingOn) await this.stopScreenShare();

    if (withVideo) {
      this.screenShareMediaStream = await this.deviceController.getMediaStream(false, true);
      this.localScreenShareStream = markRaw(new LocalVideoStream(this.screenShareMediaStream));
    } else {
      this.screenShareMediaStream = await this.deviceController.getScreenShare();
      this.localScreenShareStream = new LocalVideoStream(this.screenShareMediaStream);
    }

    await this.meeting.startScreenSharing(this.localScreenShareStream);
    
    const participant = this.data.participants.find(participant => participant.localParticipant ?? false);
    if (participant) participant.screenShareAvailable = true;
  }

  async stopScreenShare() {
    const participant = this.data.participants.find(participant => participant.localParticipant ?? false);
    if (participant) participant.screenShareAvailable = false;

    await this.meeting.stopScreenSharing(this.localScreenShareStream);
    this.localScreenShareStream = null;
    (this.screenShareMediaStream?.getVideoTracks() ?? []).forEach(track => track.stop());
    this.screenShareMediaStream = null;
  }

  async startVideo() {
    this.videoMediaStream = await this.deviceController.getMediaStream(false, true);
    this.localVideoStream = markRaw(new LocalVideoStream(this.videoMediaStream));
    await this.meeting.startVideo(this.localVideoStream);

    const participant = this.data.participants.find(participant => participant.localParticipant ?? false);
    if (participant) participant.videoAvailable = true;
  }

  async stopVideo() {
    const participant = this.data.participants.find(participant => participant.localParticipant ?? false);
    if (participant) participant.videoAvailable = false;

    await this.meeting.stopVideo(this.localVideoStream);
    this.localVideoStream = null;
    (this.videoMediaStream?.getVideoTracks() ?? []).forEach(track => track.stop());
    this.videoMediaStream = null;
  }

  async toggleMute() {
    if (this.meeting.isMuted) {
      await this.meeting.unmute();
    } else {
      await this.meeting.mute();
    }
  }

  sendStartScreenShare(withVideo = false) {
    this._dataChannelManager.sendStartScreenShare(withVideo);
    this.startScreenShare(withVideo);
  }

  // ---------- DATA CHANNEL ----------

  _createDataChannel() {
    this._dataChannelManager = new DataChannelManager(this.meeting, this.startScreenShare.bind(this));
  }

  // ---------- EVENTS & EVENT HANDLERS ----------

  /** Registers event handlers on the Call object */
  _registerEventHandlers() {
    this._addEventHandler('stateChanged', this._onStateChanged.bind(this));
    this._addEventHandler('remoteParticipantsUpdated', this._onRemoteParticipantsUpdated.bind(this));
    // this._addEventHandler('localAudioStreamsUpdated', this._onLocalAudioStreamsUpdated.bind(this));
    this._addEventHandler('localVideoStreamsUpdated', this._onLocalVideoStreamsUpdated.bind(this));
    this._addEventHandler('isScreenSharingOnChanged', this._onIsScreenSharingOnChanged.bind(this));
    this._addEventHandler('isMutedChanged', this._onIsMutedChanged.bind(this));
  }

  _onLocalVideoStreamsUpdated({ added, removed } = {}) {
    console.log('🛑 - _onLocalVideoStreamsUpdated - added', added, 'removed', removed);

    if (removed?.length > 0) {
      removed.forEach(stream => {
        // For only a screen share, clean up the local screen share stream & MediaStream. This means that the screen share has been stopped remotely.
        // This can happen if two folks start a screen share at the same time. One will 'win' and the other will be stopped.
        if (stream === this.localScreenShareStream) {
          this.localScreenShareStream = null;
          (this.screenShareMediaStream?.getVideoTracks() ?? []).forEach(track => track.stop());
          this.screenShareMediaStream = null;
        }
      });
    }
  }

  _onIsScreenSharingOnChanged() {
    console.debug(`_onIsScreenSharingChanged isScreenSharingOn-[${this.meeting.isScreenSharingOn}]`);

    const participant = this.data.participants.find(participant => participant.localParticipant ?? false);
    if (participant) participant.screenShareAvailable = this.meeting.isScreenSharingOn;
  }

  _onIsMutedChanged() {
    const isMuted = this.meeting.isMuted;
    console.debug(`_onIsMuteChanged isMuted-[${isMuted}]`);
    const participant = (this.data.participants ?? [])?.find(({ localParticipant }) => localParticipant);

    participant.isMuted = isMuted;
  }

  /** Registers a single event handler and stores the name/listener so it can be removed */
  _addEventHandler(name, listener) {
    this.meeting.on(name, listener);
    this._eventHandlers.push({ name, listener });
  }

  /** Removes all event handlers */
  _removeEventHandlers() {
    this._eventHandlers.forEach(handler => this.meeting.off(handler.name, handler.listener));
  }

  _onStateChanged() {
    this.data.state = this.meeting.state;
    console.debug(`MS_TeamsRoom._onStateChanged() - state-[${this.data.state}]`);

    const states = Object.freeze({
      Connected: this._onStateConnected.bind(this),
      Disconnected: this._onStateDisconnected.bind(this)
    });

    states[this.meeting.state]?.();
  }

  _onStateConnected() {
    const isMuted = this.meeting.isMuted;
    console.debug(`MS_TeamsRoom._onStateConnected() isMuted-[${isMuted}]`);

    this.data.participants.push({
      localParticipant: true,
      name: null,
      user: this.msTeamsController.getClientId(),
      videoAvailable: (this.videoMediaStream?.getVideoTracks?.() ?? []).length > 0,
      screenShareAvailable: false,
      isMuted
    });

    this.addRemoteParticipants(this.meeting?.remoteParticipants);
  }

  _onStateDisconnected() {
    this._removeEventHandlers();
  }

  _onRemoteParticipantsUpdated(event) {
    const state = this.meeting.state;
    const { added = [], removed = [] } = event;

    console.debug(`MS_TeamsRoom._onRemoteParticipantsUpdated() state-[${state}] - added`, added, 'removed', removed);

    // Ignore if we are in the lobby
    if (state === 'InLobby') return;

    this.addRemoteParticipants(added);
    this.removeRemoteParticipants(removed);
  }

  addRemoteParticipants(added = []) {
    console.debug(`MS_TeamsRoom.addRemoteParticipants() - added`, added);

    added.forEach(participant => {
      const participantId = RemoteParticipant.ParticipantId(participant);

      // Already exists
      if (this.remoteParticipants.has(participantId)) return;

      const p = new RemoteParticipant(participant, this._updateCb.bind(this));
      console.debug(`MS_TeamsRoom.addRemoteParticipants() - p`, p);
      this.remoteParticipants.set(p.id, p);

      this.data.participants.push({
        id: p.id,
        videoAvailable: p.videoAvailable,
        screenShareAvailable: p.screenShareAvailable
      })
    });
  }

  removeRemoteParticipants(removed = []) {
    console.debug(`MS_TeamsRoom.removeRemoteParticipants() - removed`, removed);

    removed.forEach(participant => {
      const participantId = RemoteParticipant.ParticipantId(participant);
      console.debug(`MS_TeamsRoom.removeRemoteParticipants() - participantId-[${participantId}] participant`, participant);

      const idx = this.data.participants.findIndex(p => p.id === participantId);
      console.debug(`MS_TeamsRoom.removeRemoteParticipants() - idx-[${idx}]`);
      if (idx !== -1) {
        this.data.participants.splice(idx, 1);
      }
      this.remoteParticipants.delete(participantId);
    });
  }

  async getAudioInputDevices() {
    this.data.audioInputDevices = await this.deviceController.getAudioInputDevices();
  }

  _updateCb(participant) {
    const p = this.data.participants.find(p => p.id === participant.id);
    if (!p) return;

    p.id = participant.id;
    p.displayName = participant.displayName;
    p.state = participant.state;
    p.videoAvailable = participant.videoAvailable;
    p.screenShareAvailable = participant.screenShareAvailable;
    p.participant = participant;
  }
}

const ROOM = Symbol();

export { MSTeamsRoom, ROOM };
