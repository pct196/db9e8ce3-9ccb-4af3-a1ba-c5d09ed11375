// Configuration
require('dotenv').config();

console.log(`process.version-[${process.version}]`);

const express = require("express");
const cors = require('cors');
const ViteExpress = require("vite-express");
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const morgan = require('morgan');

const COOKIE_NAME = process.env.VITE_COOKIE_NAME;

const app = express();
// app.use(morgan(':method :url :status'));

app.use(cors({
  origin: '*'
}));

app.use(cookieParser());
app.use(bodyParser.json());

app.use((req, res, next) => {
  const cookieStr = req?.cookies[COOKIE_NAME];
  if (cookieStr) {
    try {
      const obj = JSON.parse(cookieStr);
      console.log(obj);
      if (obj) req.username = obj.username;
    } catch { /* ignore */ }
  }
  next();
});

app.post('/set-username', morgan(':method :url :status'), (req, res) => {
  const username = req.body.username;
  const name = req.body.name;
  const expires = new Date(Date.now() + 30 * 24 * 60 * 60 * 1000); // 30 days from now
  res.cookie(COOKIE_NAME, JSON.stringify({ username, name }), { expires });
  res.send({ username, name });
});

app.get('/test', morgan(':method :url :status'), (req, res) => {
  console.log(req.cookies);
  console.log(`req.username-[${req.username}]`);
  res.send();
});

app.use('/api/auth', morgan(':method :url :status'), require('./routes/auth'));

ViteExpress.listen(app, 3000, () =>
  console.log("Server is listening on port 3000...")
);
