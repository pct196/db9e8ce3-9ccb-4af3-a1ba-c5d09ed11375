const express = require('express');
const router = express.Router();

// MS
const { CommunicationIdentityClient } = require('@azure/communication-identity');
const msal = require('@azure/msal-node');

const REDIRECT_URI = process.env.REDIRECT_URI;
const clientId = process.env.CLIENT_ID;
const clientSecret = process.env.CLIENT_SECRET;
const authority = process.env.AUTHORITY;
const COMMUNICATION_SERVICES_CONNECTION_STRING = process.env.COMMUNICATION_SERVICES_CONNECTION_STRING;

console.log(`REDIRECT_URI-[${REDIRECT_URI}]`);

// Create configuration object that will be passed to MSAL instance on creation.
const msalConfig = {
  auth: {
    clientId,
    clientSecret,
    authority,
    knownAuthorities: [authority]
  },
  system: {
    loggerOptions: {
      loggerCallback(loglevel, message) {
        console.log(loglevel, message);
      },
      piiLoggingEnabled: false,
      logLevel: msal.LogLevel.Verbose
    }
  }
};

console.log('msalConfig', {msalConfig});

// Create an instance of PublicClientApplication
const cca = new msal.ConfidentialClientApplication(msalConfig);
// const provider = new CryptoProvider();

const msalTokenCache = cca.getTokenCache();
console.log(msalTokenCache);

// Map<string «Betr user», string «homeAccountId»>
const betrUserToTeamsUserMap = new Map();

// let pkceVerifier = '';
const scopes = [
  'https://graph.microsoft.com/User.Read'
];

const teamsScopes = [
  'https://auth.msft.communication.azure.com/Teams.ManageCalls',
  'https://auth.msft.communication.azure.com/Teams.ManageChats'
];

const graphScopes = [
  'https://graph.microsoft.com/Calendars.Read',
  'https://graph.microsoft.com/offline_access',
  'https://graph.microsoft.com/openid',
  'https://graph.microsoft.com/profile'
];

router.get('/', async (req, res) => {
  // Generate PKCE Codes before starting the authorization flow
  // const { verifier, challenge } = await provider.generatePkceCodes();
  // pkceVerifier = verifier;

  const authCodeUrlParameters = {
    scopes: [ ...scopes, ...teamsScopes, ...graphScopes ],
    redirectUri: REDIRECT_URI
    // codeChallenge: challenge,
    // codeChallengeMethod: 'S256'
  };

  // Get url to sign user in and consent to scopes needed for application
  try {
    console.log('before cca.getAuthCodeUrl', {authCodeUrlParameters});
    const response = await cca.getAuthCodeUrl(authCodeUrlParameters);
    console.log('response', response);
    res.send({ authUrl: response });
  } catch (error) {
    console.log(JSON.stringify(error, null, 2));
  }
});

router.get('/redirect', async (req, res) => {
  console.log({ query: req.query });

  // Create request parameters object for acquiring the AAD token and object ID of a Teams user
  const tokenRequest = {
    code: req.query.code,
    scopes: [ ...scopes ],
    redirectUri: REDIRECT_URI
  };

  console.log('tokenRequest', tokenRequest);

  try {
    const response = await cca.acquireTokenByCode(tokenRequest);
    console.log('Response:', response);

    betrUserToTeamsUserMap.set(req.username, response?.account.homeAccountId);

    // res.send({ response });
		res.redirect('/');
  } catch (error) {
    console.log(error);
    res.status(500).send({ error });
  }
});

router.get('/test', async (req, res) => {
  try {
    // This code demonstrates how to fetch your connection string
    // from an environment variable.
    const connectionString = COMMUNICATION_SERVICES_CONNECTION_STRING;

    // Instantiate the identity client
    const identityClient = new CommunicationIdentityClient(connectionString);

    const { token } = await identityClient.createUserAndToken(['voip'], { tokenExpiresInMinutes: 60 })
    console.log('token', token);

    res.send({ token });
  } catch (error) {
    console.debug(error);
    res.status(500).send({ error: error.message });
  }
});

router.get('/check', async (req, res) => {
  try {
    const homeAccountId = betrUserToTeamsUserMap.get(req.username);
    if (!homeAccountId) throw new Error('💣 - no homeAccountId');    
    
    console.log(`homeAccountId-[${homeAccountId}]`);

    const account = await msalTokenCache.getAccountByHomeId(homeAccountId);
    console.log('account', account);
    if (!account) throw new Error('💣 - no accounts');

    const teamsResponse = await cca.acquireTokenSilent({
      account,
      scopes: teamsScopes
    });

    console.log('Teams:', teamsResponse);
    res.send();
  } catch (error) {
    console.debug(error);
    res.status(401).send({ error: error.message });
  }
});

router.get('/token', async (req, res) => {
  try {
    const homeAccountId = betrUserToTeamsUserMap.get(req.username);
    if (!homeAccountId) throw new Error('💣 - no homeAccountId');

    console.log(`homeAccountId-[${homeAccountId}]`);

    const account = await msalTokenCache.getAccountByHomeId(homeAccountId);
    console.log('account', account);
    if (!account) throw new Error('💣 - no accounts');

    const teamsResponse = await cca.acquireTokenSilent({
      account,
      scopes: teamsScopes
    });
    console.log('Teams:', teamsResponse);

    const teamsUserAadToken = teamsResponse.accessToken;
    const userObjectId = teamsResponse.uniqueId;

    // This code demonstrates how to fetch your connection string
    // from an environment variable.
    const connectionString = COMMUNICATION_SERVICES_CONNECTION_STRING;

    // Instantiate the identity client
    const identityClient = new CommunicationIdentityClient(connectionString);

    // Exchange the Azure AD access token of the Teams User for a Communication Identity access token
    const accessToken = await identityClient.getTokenForTeamsUser({
      teamsUserAadToken: teamsUserAadToken,
      clientId: clientId,
      userObjectId: userObjectId
    });
    console.log('Token:', accessToken);

    res.send({
      teamsResponse,
      accessToken
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({ error: error.message });
  }
});

module.exports = router;
